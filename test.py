import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from scipy import misc
from scipy.signal import argrelextrema
from scipy.special import zeta

fig, axarr = plt.subplots(3)
sZeta=1.3
sGeom=1.02
nRange=np.arange(1,100)
zRange = np.array([1/n**sZeta for n in nRange])/zeta(sZeta,1)
gRange = np.array([1/sGeom**n for n in nRange])*(sGeom-1)
axarr[0].plot(nRange,zRange,'b')
axarr[0].plot(nRange,gRange,'r')

axarr[1].plot(nRange,np.cumsum(zRange),'b')
axarr[1].plot(nRange,np.cumsum(gRange),'r')

#axarr[0].xlabel('n')
#axarr[0].ylabel('fun')
axarr[0].legend(['zeta_n(s)','geometric_n(1/s)'])


#axarr[1].xlabel('s')
#axarr[1].ylabel('fun')
axarr[1].legend(['zeta(s)','geometric(1/s)'])

sRange=np.linspace(1.1, 10, num=50)
axarr[2].plot(sRange,zeta(sRange,1)-1,'b')
axarr[2].plot(sRange,1/(sRange-1),'r')

#axarr[2].xlabel('s')
#axarr[2].ylabel('fun')
axarr[2].legend(['zeta(s)','geometric(1/s)'])

plt.show()