import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from scipy import misc
from scipy.signal import argrelextrema

# load image and convert to grayscale
filename = 'picture1'
#filename = 'picture2'
#filename = 'picture3'
#filename = 'picture4'
#filename = 'pictureMario'

filetype = '.jpg'
imageFile = misc.imread(filename+filetype)

imageArray = np.array(imageFile)
imageArray = imageArray.astype(int)
if(len(imageFile.shape))==3: #RGB
    heigth,width,nRGB = imageFile.shape
    imageArray = np.dot(imageArray,[0.299, 0.587, 0.114])
else:
    heigth,width = imageFile.shape

#number of shifts
shiftRange = range(round(width/20),round(width/4))#
#shiftRange = range(1,round(width))
darknessFactor = np.zeros(np.shape(shiftRange))

for idx,shift in enumerate(shiftRange):
    imageShifted = imageArray.astype(int)
    imageShifted[:,shift:] = abs(imageArray[:,shift:]-imageArray[:,0:-shift])
    darknessFactor[idx]=sum(sum(imageShifted[:,shift:]))/(heigth*(width-shift))

# find minima
minimaIdx = argrelextrema(darknessFactor, np.less)
darknessFactorMinima = darknessFactor[minimaIdx]
shiftRangeMinima = np.asarray(shiftRange)[minimaIdx]

indices = darknessFactorMinima.argsort()
threshold=0.92*(darknessFactor[0:round(len(darknessFactor)/7)].mean()+darknessFactor[-round(len(darknessFactor)/7):].mean())/2
indices = indices[darknessFactorMinima[indices]<threshold]

plt.plot(shiftRange, darknessFactor)
plt.plot(shiftRange[0:round(len(darknessFactor)/7)], darknessFactor[0:round(len(darknessFactor)/7)],'g')
plt.plot(shiftRange[-round(len(darknessFactor)/7):], darknessFactor[-round(len(darknessFactor)/7):],'g')
plt.plot(shiftRangeMinima[indices], darknessFactorMinima[indices],'rx',markeredgewidth=3,markersize=10)
plt.plot(shiftRangeMinima[[0,-1]], threshold*np.ones(2),'k--')
plt.show()

# generate images for minima and show best result
for idx in indices:
    shift = shiftRangeMinima[idx]
    imageShifted = imageArray.astype(int)
    imageShifted[:,shift:] = abs(imageArray[:,shift:]-imageArray[:,0:-shift])
    imageShifted=imageShifted.astype(np.uint8)
    imageOut = Image.fromarray(imageShifted)
    imageOut.save('out/'+filename+'_'+str(shiftRange[idx])+filetype)
    if idx==indices[0]:
        imageOut.show()
